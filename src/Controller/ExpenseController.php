<?php

namespace App\Controller;

use App\Form\FilterType;
use App\Form\UploadFileType;
use App\Repository\ExpenseRepository;
use App\Repository\VehicleRepository;
use App\Service\UploadFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExpenseController extends AbstractController
{
    /**
     * @Route("/expense", name="app_expense")
     */
    public function index(Request $request, UploadFile $service, ExpenseRepository $expenseRepository, VehicleRepository $vehicleRepository): Response
    {
        $form = $this->createForm(UploadFileType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $expenseFile = $form->get('expense_file')->getData();
            $service->saveData($expenseFile);

            return $this->redirectToRoute('app_expense');
        }

        $filterForm = $this->createForm(FilterType::class);
        $filterForm->handleRequest($request);
        $filters = [];
        if ($filterForm->isSubmitted()) {
            $filters = $filterForm->getData();
        }
        $dataTop10 = $vehicleRepository->getTop10($filters);
        $dataSum = $expenseRepository->getExpensesSum($filters);
        //dd($dataTop10, $dataSum);

        return $this->render('expense/index.html.twig', [
            'form' => $form->createView(),
            'filterForm' => $filterForm->createView(),
            'dataTop10' => $dataTop10,
            'dataSum' => $dataSum
        ]);
    }
}
