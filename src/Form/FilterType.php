<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('start_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date début',
                'data' => new \DateTime(),
            ])
            ->add('end_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date fin',
                'data' => new \DateTime(),
            ])
            ->add('price_mode', ChoiceType::class, [
                'choices'  => [
                    'TTC' => 'ttc',
                    'HT' => 'ht',
                ],
            ])
            ->add('apply_filter', CheckboxType::class, [
                'label' => 'Appliquer les filtres',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
