<?php

namespace App\Geo;


class Point
{
    private $latitude;
    private $longitude;
    public function __construct($latitude, $longitude)
    {
        $this->latitude  = floatval($latitude);
        $this->longitude = floatval($longitude);
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }
}