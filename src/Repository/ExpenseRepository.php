<?php

namespace App\Repository;

use App\Entity\Expense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Expense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expense[]    findAll()
 * @method Expense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Expense::class);
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getExpensesSum($filters)
    {
        $startDate = $filters['start_date'] ?? null;
        $endDate = $filters['end_date'] ?? null;
        $priceMode = $filters['price_mode'] ?? 'ttc';
        $applyFilter = $filters['apply_filter'] ?? false;

        $qb = $this->createQueryBuilder('exp');
        $qb->select('exp.category');
        if ($priceMode === 'ttc') {
            $qb->addSelect('exp.valueTi as price');
        } else {
            $qb->addSelect('exp.valueTe as price');
        }
        if ($applyFilter) {
            if (!is_null($startDate)) {
                $qb->andWhere(
                    $qb->expr()->gte('exp.issuedOn', ':start')
                );
                $qb->setParameter('start', $startDate);
            }
            if (!is_null($endDate)) {
                $qb->andWhere(
                    $qb->expr()->lte('exp.issuedOn', ':end')
                );
                $qb->setParameter('end', $endDate->setTime(23, 59, 59));
            }
        }
        $data = $qb->getQuery()->getResult();
        $sumByCategory = [];
        foreach ($data as $expense) {
            if (isset($sumByCategory[$expense['category']])) {
                $sumByCategory[$expense['category']] += $expense['price'];
            } else {
                $sumByCategory[$expense['category']] = $expense['price'];
            }
        }

        return [
            'sum' => array_sum($sumByCategory),
            'sumByCategory' => $sumByCategory
        ];
    }
}
