<?php

namespace App\Repository;

use App\Entity\Expense;
use App\Entity\Vehicle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vehicle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vehicle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vehicle[]    findAll()
 * @method Vehicle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vehicle::class);
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function getTop10($filters)
    {
        $startDate = $filters['start_date'] ?? null;
        $endDate = $filters['end_date'] ?? null;
        $priceMode = $filters['price_mode'] ?? 'ttc';
        $applyFilter = $filters['apply_filter'] ?? false;

        $qb = $this->createQueryBuilder('ve');
        $qb->innerJoin(
            Expense::class,
            'exp',
            'WITH',
            'exp.vehicle = ve.vehicleId'
        );

        $qb->select('ve.plateNumber');
        if ($priceMode === 'ttc') {
            $qb->addSelect('sum(exp.valueTi) as expenseSum');
        } else {
            $qb->addSelect('sum(exp.valueTe) as expenseSum');
        }
        if ($applyFilter) {
            if (!is_null($startDate)) {
                $qb->andWhere(
                    $qb->expr()->gte('exp.issuedOn', ':start')
                );
                $qb->setParameter('start', $startDate);
            }
            if (!is_null($endDate)) {
                $qb->andWhere(
                    $qb->expr()->lte('exp.issuedOn', ':end')
                );
                $qb->setParameter('end', $endDate->setTime(23, 59, 59));
            }
        }
        $qb->groupBy('ve.plateNumber');
        $qb->orderBy('expenseSum', 'DESC');
        $qb->setMaxResults(10);

        return $qb->getQuery()->getResult();
    }
}
