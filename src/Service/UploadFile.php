<?php

namespace App\Service;


use App\Entity\Expense;
use App\Entity\GasStation;
use App\Entity\Vehicle;
use App\Geo\Point;
use App\Repository\ExpenseRepository;
use App\Repository\VehicleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class UploadFile
{
    private $em;
    private $vehicleRepository;
    private $expenseRepository;

    public function __construct(EntityManagerInterface $em, VehicleRepository $vehicleRepository, ExpenseRepository $expenseRepository)
    {
        $this->em = $em;
        $this->vehicleRepository = $vehicleRepository;
        $this->expenseRepository = $expenseRepository;
    }

    public function saveData(UploadedFile $file) {
        if (($handle = fopen($file->getPathname(), "r")) !== false)
        {
            $count = 0;
            $batchSize = 1000;

            while (($data = fgetcsv($handle, 0, ";")) !== false)
            {
                if ($count > 0) {
                    $violations = $this->validateData($data);
                    if ($violations->count() > 0) {
                        continue;
                    }
                    $vehicle = $this->vehicleRepository->findOneBy(['plateNumber' => $data[0]]);
                    if (!$vehicle instanceof Vehicle) {
                        $vehicle = new Vehicle();
                        $vehicle->setPlateNumber($data[0]);
                        $vehicle->setBrand($data[1]);
                        $vehicle->setModel($data[2]);
                        $this->em->persist($vehicle);
                    }

                    $expense = $this->expenseRepository->findOneBy(['expenseNumber' => $data[10]]);
                    if (!$expense instanceof Expense) {
                        $expense = new Expense();
                        $expense->setVehicle($vehicle);
                        $expense->setCategory($data[3]);
                        $expense->setValueTe(floatval($data[5]));
                        $expense->setValueTi(floatval($data[6]));
                        $expense->setTaxRate(floatval($data[7]));
                        $expense->setIssuedOn(new \DateTime($data[8]));
                        $expense->setInvoiceNumber($data[9]);
                        $expense->setExpenseNumber($data[10]);
                        $this->em->persist($expense);
                    }

                    $gasStation = new GasStation();
                    $gasStation->setExpense($expense);
                    $gasStation->setDescription($data[11]);
                    $gasStation->setCoordinate(new Point($data[12], $data[13]));
                    $this->em->persist($gasStation);

                    if (($count % $batchSize) === 0 )
                    {
                        $this->em->flush();
                        $this->em->clear();
                    }
                }
                $count++;
            }
            fclose($handle);
            $this->em->flush();
            $this->em->clear();
        }die;
    }

    public function validateData($data)
    {
        $validator = Validation::createValidator();
        $constraint = new Assert\Collection([
            0 => new Assert\NotBlank(),
            1 => new Assert\NotBlank(),
            2 => new Assert\NotBlank(),
            3 => new Assert\Choice(['gasoline','diesel','electricity_charge','gpl','hydrogen']),
            4 => new Assert\NotBlank(),
            5 => new Assert\NotBlank(),
            6 => new Assert\NotBlank(),
            7 => new Assert\NotBlank(),
            8 => new Assert\NotBlank(),
            9 => new Assert\NotBlank(),
            10 => new Assert\NotBlank(),
            11 => new Assert\NotBlank(),
            12 => new Assert\NotBlank(),
            13 => new Assert\NotBlank(),
        ]);

        return $validator->validate($data, $constraint);
    }
}